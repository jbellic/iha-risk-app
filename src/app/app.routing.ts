import {ModuleWithProviders} from '@angular/core';
import {RouterModule, Routes} from '@angular/router';
import {AdminComponent} from './admin/admin.component';
import {WizardComponent} from './wizard/wizard.component';
import {AuthGuard} from './common/auth.guard';
import {ErrorComponent} from './wizard/error/error.component';

const appRoutes: Routes = [
  {path: '', component: WizardComponent, canActivate: [AuthGuard]},
  {path: 'admin', component: AdminComponent, canActivate: [AuthGuard]},
  {path: 'wizard', component: WizardComponent, canActivate: [AuthGuard]},
  {path: 'error', component: ErrorComponent},
  {path: '**', redirectTo: 'wizard', canActivate: [AuthGuard]}
];

export const routing: ModuleWithProviders = RouterModule.forRoot(appRoutes);

