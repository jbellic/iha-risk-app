import {Component, OnInit} from '@angular/core';
import {CategoryComponent} from '../wizard/category/category.component';
import {CategoryService} from 'app/services/category.service';
import {QuestionComponent} from '../wizard/category/question/question.component';
import {OptionComponent} from '../wizard/category/question/option/option.component';
import {getUuidV4String} from '../shared/uuid.util';
import {ModifierComponent} from '../wizard/category/question/option/modifier/modifier.component';
import {ContentService} from '../services/content.service';
import {ContentComponent} from '../wizard/content/content.component';

@Component({
  selector: 'app-admin',
  templateUrl: './admin.component.html',
  styleUrls: ['./admin.component.css'],
})

export class AdminComponent implements OnInit {

  public categories: CategoryComponent[];
  public questions: QuestionComponent[];
  public title = 'IHA Admin bölümü';
  public categoryButton: boolean;
  public saveButton: boolean;
  public resetButton: boolean;
  public successThresholdNumber: ContentComponent;
  public successThresholdText: ContentComponent;
  public failureThresholdText: ContentComponent;

  static orderByDate(collection) {
    if (collection) {
      collection.sort((a: any, b: any) => {
        if (a.createdAt < b.createdAt) {
          return -1;
        } else if (a.createdAt > b.createdAt) {
          return 1;
        } else {
          return 0;
        }
      });
    }
  }

  constructor(private categoryService: CategoryService, private contentService: ContentService) {
  }

  ngOnInit(): void {

    const thresholdKeys = ['successThresholdText', 'failureThresholdText', 'successThresholdNumber'];

    this.successThresholdNumber = new ContentComponent('', '');
    this.successThresholdText = new ContentComponent('', '');
    this.failureThresholdText = new ContentComponent('', '');


    this.contentService
      .getContent(thresholdKeys)
      .then(result => this.handleThresholdResult(result))
      .catch(error => console.log(error));

    this.categoryService
      .getCategories()
      .then(result => this.categories = result)
      .then(result => this.sortCategoriesByDate(this.categories))
      .then(result => this.extractQuestions())
      .catch(error => console.log(error));
  }

  public handleThresholdResult(result: ContentComponent[]) {
    for (const content of result) {
      if (content.name === 'successThresholdText') {
        this.successThresholdText = content;
      } else if (content.name === 'failureThresholdText') {
        this.failureThresholdText = content;
      } else if (content.name === 'successThresholdNumber') {
        this.successThresholdNumber = content
      }
    }
  }

  public sortCategoriesByDate(categories) {
    AdminComponent.orderByDate(this.categories);
    for (const category of this.categories) {
      AdminComponent.orderByDate(category.questions);
    }
  }

  public extractQuestions(): void {
    const questions: QuestionComponent[] = [];
    for (const category of this.categories) {
      for (const question of category.questions) {
        questions.push(question);
      }
    }
    this.questions = questions;
  }

  public addCategory() {
    const uuid = getUuidV4String();
    this.categories.push(new CategoryComponent(uuid, 'Yeni Kategori', 'Kategori Tanim'))
  }

  public removeCategory(categoryId) {
    let categoryIndex;
    this.categories.forEach((item, index) => {
      if (item.id === categoryId) {
        categoryIndex = index;
      }
    });
    this.categoryService.removeCategory(categoryId);
    this.categories.splice(categoryIndex, 1);
    this.extractQuestions();
  }

  public saveCategories() {
    this.categoryButton = true;
    this.saveButton = true;
    this.resetButton = true;

    this.categoryService.saveCategories(this.categories);

    const contents = [this.successThresholdText, this.failureThresholdText, this.successThresholdNumber];
    this.contentService.saveContent(JSON.stringify(contents));

    setTimeout(() => {
        this.categoryButton = false;
        this.saveButton = false;
        this.resetButton = false;
      },
      3000);
  }

  public resetCategories() {
    this.resetButton = true;
    this.categoryButton = true;
    this.saveButton = true;

    this.ngOnInit();

    setTimeout(() => {
        this.resetButton = false;
        this.categoryButton = false;
        this.saveButton = false;
      },
      3000);
  }

  public addQuestion(categoryId) {
    for (const category of this.categories) {
      if (category.id === categoryId) {
        category.questions.push(new QuestionComponent(getUuidV4String(), categoryId))
      }
    }
    this.extractQuestions();
  }

  public removeQuestion(categoryId) {
    for (const category of this.categories) {
      if (category.id === categoryId) {
        category.questions.pop();
      }
    }
    this.extractQuestions();
  }

  public addOption(questionId) {
    const uuid = questionId;
    for (const c of this.categories) {
      for (const q of c.questions) {
        if (q.id === questionId) {
          const option = new OptionComponent(getUuidV4String());
          option.questionId = questionId;
          q.options.push(option);
        }
      }
    }
  }

  public removeOption(questionId) {
    for (const category of this.categories) {
      for (const question of category.questions) {
        if (question.id === questionId) {
          question.options.pop()
        }
      }
    }
  }

  public addModifier(optionId) {
    const uuid = getUuidV4String();
    for (const c of this.categories) {
      for (const q of c.questions) {
        for (const o of q.options) {
          if (o.id === optionId) {
            const modifier = new ModifierComponent(getUuidV4String());
            modifier.optionId = optionId;
            o.modifiers.push(modifier);
          }
        }
      }
    }
  }

  public removeModifier(optionId) {
    for (const category of this.categories) {
      for (const question of category.questions) {
        for (const option of question.options) {
          if (option.id === optionId) {
            option.modifiers.pop()
          }
        }
      }
    }
  }
}

