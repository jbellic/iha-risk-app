import {Injectable} from '@angular/core';
import {Http, Response, Headers, RequestOptions} from '@angular/http';
import {Observable} from 'rxjs/Observable';
import 'rxjs/add/operator/map';
import 'rxjs/add/operator/toPromise';
import 'rxjs/add/operator/catch';
import 'rxjs/add/observable/throw';
import {CanActivate, Router} from '@angular/router';
import {SlimLoadingBarService} from "ng4-slim-loading-bar";

@Injectable()
export class AuthGuard implements CanActivate {
  private headers: Headers;
  private options: RequestOptions;
  private url = 'http://iha-risk-backend-net.azurewebsites.net/api/result';

  constructor(private router: Router, private http: Http, private slimLoadingBarService: SlimLoadingBarService) {
    this.headers = new Headers({'Content-Type': 'application/json', 'Accept': 'q=0.8;application/json;q=0.9'});
    this.options = new RequestOptions({headers: this.headers});
    this.slimLoadingBarService.interval = 25;
    this.slimLoadingBarService.start();
  }

  getTokenLifeCycle(token): Observable<boolean> {
    const localData = localStorage.setItem('token', token);
    return this.http
      .get(this.url + '/' + token, this.options)
      .map((res: Response)=> {
        if (res.json() == true){
          return true;
        }
        else
          this.router.navigate(['/error']);
        this.slimLoadingBarService.complete();
        return false;
      })
      .catch(this.handleError);

  }

  canActivate() {
    const urlParams = new URLSearchParams(window.location.search);
    const token = urlParams.get('token');

    if (typeof token !== 'undefined' && token && this.isUUID(token))
      return this.getTokenLifeCycle(token);
    else
      this.router.navigate(['/error']);
    this.slimLoadingBarService.complete();
  }

  private isUUID(s){
    const pattern =  /^(\{){0,1}[0-9a-fA-F]{8}\-[0-9a-fA-F]{4}\-[0-9a-fA-F]{4}\-[0-9a-fA-F]{4}\-[0-9a-fA-F]{12}(\}){0,1}$/gi;
    return pattern.test(s);
  }

  private handleError(error: any) {
    const errMsg = (error.message) ? error.message :
      error.status ? `${error.status} - ${error.statusText}` : 'Server error';
    console.error(errMsg);
    return Observable.throw(errMsg);
  }

}
