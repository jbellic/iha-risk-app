import {BrowserModule} from '@angular/platform-browser';
import {NgModule} from '@angular/core';
import {FormsModule} from '@angular/forms';
import {HttpModule} from '@angular/http';
import {routing} from './app.routing';
import {HashLocationStrategy, LocationStrategy} from '@angular/common';
import {AppComponent} from './app.component';
import {WizardComponent} from './wizard/wizard.component';
import {CategoryService} from './services/category.service';
import {ResultComponent} from './wizard/result/result.component';
import {AdminComponent} from './admin/admin.component';
import {DatePipe} from '@angular/common';
import {ResultService} from './services/result.service';
import {OrderByPipe} from './shared/orderby.pipe';
import {IntroComponent} from 'app/wizard/intro/intro.component';
import {ContentService} from 'app/services/content.service';
import {AuthGuard} from './common/auth.guard';
import {ErrorComponent} from './wizard/error/error.component';
import {SlimLoadingBarModule} from "ng4-slim-loading-bar";


@NgModule({
  declarations: [
    AppComponent,
    WizardComponent,
    ResultComponent,
    IntroComponent,
    AdminComponent,
    ErrorComponent,
    OrderByPipe
  ],
  imports: [
    BrowserModule,
    HttpModule,
    FormsModule,
    SlimLoadingBarModule.forRoot(),
    routing
  ],
  providers: [
    CategoryService,
    ResultService,
    ContentService,
    AuthGuard,
    DatePipe,
    {provide: LocationStrategy, useClass: HashLocationStrategy}
  ],
  bootstrap: [AppComponent]
})

export class AppModule {
}
