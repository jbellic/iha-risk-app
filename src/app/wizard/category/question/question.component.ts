import {OptionComponent} from './option/option.component';

export class QuestionComponent {

  public id: string;
  public name: string;
  public categoryId: string;
  public hint: string;
  public createdAt: Date;
  public options: OptionComponent[] = [];
  public selectedOption: string;
  public visible: boolean;

  constructor(id: string, categoryId: string) {
    this.id = id;
    this.categoryId = categoryId;
    this.createdAt = new Date();
    this.visible = true;
  }
}
