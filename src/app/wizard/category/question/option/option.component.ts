import {ModifierComponent} from './modifier/modifier.component';
export class OptionComponent {

  public id: string;
  public name: string;
  public value: string;
  public factor: number;
  public questionId: string;
  public hint: string;
  public createdAt: Date;
  public modifiers: ModifierComponent[] = [];

  constructor(id: string) {
    this.id = id;
    this.createdAt = new Date();
  }
}
