export class ModifierComponent {

  public id: string;
  public optionId: string;
  public targetQuestion: string;
  public createdAt: Date;

  constructor(id: string) {
    this.id = id;
    this.createdAt = new Date();
  }
}
