import {QuestionComponent} from './question/question.component';

export class CategoryComponent {

  public id: string;
  public name: string;
  public hint: string;
  public createdAt: Date;
  public questions: QuestionComponent[] = [];

  constructor(id: string, name: string, hint: string) {
    this.id = id;
    this.name = name;
    this.hint = hint;
    this.createdAt = new Date();
  }
}
