import {Component, OnInit} from '@angular/core';
import {ContentService} from 'app/services/content.service';
import {ContentComponent} from '../content/content.component';

@Component({
  selector: 'app-intro',
  templateUrl: './intro.component.html',
  styleUrls: ['./intro.component.css']
})
export class IntroComponent implements OnInit {

  public intro: ContentComponent;

  constructor(private contentService: ContentService) {
  }

  ngOnInit() {
/*    this.contentService
      .getIntroText()
      .then(result => this.intro = result)
      .catch(error => console.log(error));*/
  }

}
