import {Component, OnInit} from '@angular/core';
import {CategoryComponent} from '../category/category.component';
import {WizardComponent} from '../wizard.component';
import {ResultService} from '../../services/result.service';
import {ContentComponent} from '../content/content.component';
import {ContentService} from '../../services/content.service';
import {element} from 'protractor';
import {ResultTransfer} from './transfer/result.transfer';

@Component({
  selector: 'app-result',
  templateUrl: './result.component.html',
  styleUrls: ['./result.component.css'],
  providers: [WizardComponent]
})
export class ResultComponent implements OnInit {

  public successful: boolean;
  public result: number;
  public resultReview: string;
  public resultReviewHints: string[];
  public resultText: string;
  public successThresholdNumber: ContentComponent;
  public successThresholdText: ContentComponent;
  public failureThresholdText: ContentComponent;

  static round(value, precision) {
    const multiplier = Math.pow(10, precision || 0);
    return Math.round(value * multiplier) / multiplier;
  }

  constructor(private resultService: ResultService, private contentService: ContentService) {
  }

  ngOnInit(): void {
    const thresholdKeys = ['successThresholdText', 'failureThresholdText', 'successThresholdNumber'];
    this.resultText = 'Toplam puan: ';
    this.successful = true;
    this.contentService
      .getContent(thresholdKeys)
      .then(result => this.handleThresholdResult(result))
      .catch(error => console.log(error));
  }

  public handleThresholdResult(result: ContentComponent[]) {
    for (const content of result) {
      if (content.name === 'successThresholdText') {
        this.successThresholdText = content;
      } else if (content.name === 'failureThresholdText') {
        this.failureThresholdText = content;
      } else if (content.name === 'successThresholdNumber') {
        this.successThresholdNumber = content
      }
    }
  }

  public calculateAll(): void {
    this.result = Number(0);
    const categories: CategoryComponent[] = this.resultService.categories;
    for (const category of categories) {
      for (const question of category.questions) {
        if (question.visible) {
          for (const option of question.options) {
            if (option.id === question.selectedOption) {
              this.result += Number(option.factor);
              this.result = ResultComponent.round(this.result, 4);
            }
          }
        }
      }
    }
    this.result = ResultComponent.round(this.result * 40, 2);
    if (Number(this.result) <= Number(this.successThresholdNumber.value)) {
      this.resultReview = this.successThresholdText.value;
      this.resultReviewHints = this.extractMatchingHints();
      this.successful = true;
    } else {
      this.resultReview = this.failureThresholdText.value;
      this.resultReviewHints = null;
      this.successful = false;
    }

    const token = localStorage.getItem('token');
    const resultTransfer = new ResultTransfer(token, this.successful == true ? '881' : '882', String(this.result));
    this.resultService.saveResult(resultTransfer);
  }

  public evaluateResultTextStyle(): string {
    if (this.successful) {
      return 'bg-success text-white'
    }
    return 'bg-danger';
  }

  public resultReviewRendered(): boolean {
    return this.resultReview !== undefined && this.resultReview.length > 0
  }

  public extractMatchingHints(): string[] {
    const hints: string[] = [];
    const categories: CategoryComponent[] = this.resultService.categories;

    for (const category of categories) {
      for (const question of category.questions) {
        if (question.visible) {
          for (const option of question.options) {
            if (option.id === question.selectedOption && option.factor && option.factor > Number(0.0)) {
              hints.push(option.hint)
            }
          }
        }
      }
    }
    return hints;
  }
}
