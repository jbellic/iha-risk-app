export class ResultTransfer {

  public token: string;
  public riskStatus: string;
  public riskScore: string;


  constructor(token: string, riskStatus: string, riskScore: string) {
    this.token = token;
    this.riskStatus = riskStatus;
    this.riskScore = riskScore;
  }
}
