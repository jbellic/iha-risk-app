import {getUuidV4String} from '../../shared/uuid.util';
export class ContentComponent {

  public id: string;
  public name: string;
  public value: string;
  public createdAt: Date;

  constructor(name: string, value: string) {
    this.id = getUuidV4String();
    this.name = name;
    this.value = value;
    this.createdAt = new Date();
  }
}
