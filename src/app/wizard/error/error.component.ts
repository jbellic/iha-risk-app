import {Component, OnInit} from '@angular/core';
import {ContentService} from 'app/services/content.service';
import {ContentComponent} from '../content/content.component';

@Component({
  selector: 'app-error',
  templateUrl: './error.component.html',
  styleUrls: ['./error.component.css']
})
export class ErrorComponent implements OnInit {

  public intro: ContentComponent;

  constructor(private contentService: ContentService) {
  }

  ngOnInit() {
/*    this.contentService
      .getIntroText()
      .then(result => this.intro = result)
      .catch(error => console.log(error));*/
  }

}
