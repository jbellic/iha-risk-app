import {Component, OnInit} from '@angular/core';
import {CategoryComponent} from './category/category.component';
import {CategoryService} from '../services/category.service';
import {ResultService} from '../services/result.service';
import {SlimLoadingBarService} from "ng4-slim-loading-bar";

@Component({
  selector: 'app-wizard',
  templateUrl: './wizard.component.html',
  styleUrls: ['./wizard.component.css'],
  providers: [CategoryService]
})
export class WizardComponent implements OnInit {

  public title = 'İHA risk değerlendirme formu';
  public categories: CategoryComponent[];

  public loadScript(url) {
    const node = document.createElement('script');
    node.src = url;
    node.type = 'text/javascript';
    document.getElementsByTagName('head')[0].appendChild(node);
    this.slimLoadingBarService.complete();
  }

  constructor(private categoryService: CategoryService, private resultService: ResultService, private slimLoadingBarService: SlimLoadingBarService) {
  }

  ngOnInit(): void {
    this.categoryService
      .getCategories()
      .then(result => this.categories = result)
      .then(result => this.fillVisible())
      .then(result => this.loadScript('assets/js/material-bootstrap-wizard.js'))
      .catch(error => console.log(error));
  }

  public fillVisible() {
    for (const category of this.categories) {
      for (const question of category.questions) {
        question.visible = true;
      }
    }
  }

  public isQuestionVisible(questionId): boolean {
    for (const category of this.categories) {
      for (const question of category.questions) {
        if (question.id === questionId) {
          return true;
        }
      }
    }
    return false;
  }

  public onChange(selectedOptionName, questionId) {
    for (const category of this.categories) {
      for (const question of category.questions) {
        if (question.id === questionId) {
          for (const option of question.options) {
            if (option.name === selectedOptionName) {
              question.selectedOption = option.id;
              this.disableQuestion(option.id);
            }
          }
        }
      }
    }
    this.resultService.categories = this.categories;
  }


  public disableQuestion(optionId) {
    const targetQuestions: string[] = [];
    for (const category of this.categories) {
      for (const question of category.questions) {
        for (const option of question.options) {
          if (option.id === optionId) {
            for (const modifier of option.modifiers) {
              targetQuestions.push(modifier.targetQuestion);
            }
          }
        }
      }
    }

    for (const category of this.categories) {
      for (const question of category.questions) {
        if (targetQuestions.includes(question.name)) {
          console.log('disable question: ' + question.name);
          question.visible = false;
        } else {
          question.visible = true;
        }
      }
    }
  }

  public nextButton() {

  }

  public previousButton() {

  }
}
