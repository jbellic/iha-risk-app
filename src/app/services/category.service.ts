import {Injectable} from '@angular/core';
import {Http, Response, Headers, RequestOptions} from '@angular/http';
import {Observable} from 'rxjs/Observable';
import 'rxjs/add/operator/map';
import 'rxjs/add/operator/toPromise';
import 'rxjs/add/operator/catch';
import 'rxjs/add/observable/throw';
import {CategoryComponent} from '../wizard/category/category.component';

@Injectable()
export class CategoryService {

  url = 'http://iha-risk-backend-net.azurewebsites.net/api/categories';
  headers: Headers;
  options: RequestOptions;

  constructor(private http: Http) {
    this.headers = new Headers({'Content-Type': 'application/json', 'Accept': 'q=0.8;application/json;q=0.9'});
    this.options = new RequestOptions({headers: this.headers});
  }

  getCategories(): Promise<CategoryComponent[]> {
    return this.http
      .get(this.url, this.options)
      .toPromise()
      .then(this.extractData)
      .catch(this.handleError);
  }

  saveCategories(body): Promise<CategoryComponent[]> {
    return this.http
      .post(this.url, body, this.options)
      .toPromise()
      .then(this.extractData)
      .catch(this.handleError);
  }

  removeCategory(categoryId) {
     this.http
      .delete(this.url + '/' + categoryId, this.options)
       .toPromise()
       .then(this.extractData)
      .catch(this.handleError);
  }

  private extractData(res: Response) {
    const body = res.json();
    return body || {};
  }

  private handleError(error: any) {
    const errMsg = (error.message) ? error.message :
      error.status ? `${error.status} - ${error.statusText}` : 'Server error';
    console.error(errMsg);
    return Observable.throw(errMsg);
  }
}


