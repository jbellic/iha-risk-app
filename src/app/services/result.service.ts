/**
 * for singleton injection: don't list in component providers, only modules + component constructor
 */
import {Injectable} from '@angular/core';
import {Http, Response, Headers, RequestOptions} from '@angular/http';
import {Observable} from 'rxjs/Observable';
import 'rxjs/add/operator/map';
import 'rxjs/add/operator/toPromise';
import 'rxjs/add/operator/catch';
import 'rxjs/add/observable/throw';
import {CategoryComponent} from '../wizard/category/category.component';
import {ContentComponent} from '../wizard/content/content.component';

@Injectable()
export class ResultService {

  public categories: CategoryComponent[] = [];
  public contents: ContentComponent[] = [];
  public readyForCalculation: boolean;

  private headers: Headers;
  private options: RequestOptions;
  private resultUrl = 'http://iha-risk-backend-net.azurewebsites.net/api/result';

  public saveResult(body): void {
    this.http
      .post(this.resultUrl, body, this.options)
      .toPromise()
      .catch(this.handleError);
  }

  private handleError(error: any) {
    const errMsg = (error.message) ? error.message :
      error.status ? `${error.status} - ${error.statusText}` : 'Server error';
    console.error(errMsg);
    return Observable.throw(errMsg);
  }

  constructor(private http: Http) {
    this.headers = new Headers({'Content-Type': 'application/json', 'Accept': 'q=0.8;application/json;q=0.9'});
    this.options = new RequestOptions({headers: this.headers});
  }
}
