import {Injectable} from '@angular/core';
import {Headers, Http, RequestOptions, Response} from '@angular/http';
import {Observable} from 'rxjs/Observable';
import 'rxjs/add/operator/map';
import 'rxjs/add/operator/toPromise';
import 'rxjs/add/operator/catch';
import 'rxjs/add/observable/throw';
import {ContentComponent} from '../wizard/content/content.component';

@Injectable()
export class ContentService {

  url = 'http://iha-risk-backend-net.azurewebsites.net/api/content';
  headers: Headers;
  options: RequestOptions;

  static extractData(res: Response) {
    const body = res.json();
    return body || {};
  }

  static handleError(error: any) {
    const errMsg = (error.message) ? error.message :
      error.status ? `${error.status} - ${error.statusText}` : 'Server error';
    console.error(errMsg);
    return Observable.throw(errMsg);
  }

  constructor(private http: Http) {
    this.headers = new Headers({'Content-Type': 'application/json', 'Accept': 'q=0.8;application/json;q=0.9'});
    this.options = new RequestOptions({headers: this.headers});
  }

  getContent(params: string[]): Promise<ContentComponent[]> {
    const headers = new Headers();
    headers.append('Content-Type', 'application/json');
    headers.append('Accept', 'q=0.8;application/json;q=0.9');

    const myParams = new URLSearchParams();
    for (const param of params) {
      myParams.append('param', param);
    }
    const options = new RequestOptions({headers: headers, params: myParams});

    return this.http
      .get(this.url, options)
      .toPromise()
      .then(ContentService.extractData)
      .catch(ContentService.handleError);
  }

  saveContent(body): Promise<ContentComponent[]> {
    return this.http
      .post(this.url, body, this.options)
      .toPromise()
      .then(ContentService.extractData)
      .catch(ContentService.handleError);
  }
}


