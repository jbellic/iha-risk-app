import { IhaRiskAppPage } from './app.po';

describe('iha-risk-app App', () => {
  let page: IhaRiskAppPage;

  beforeEach(() => {
    page = new IhaRiskAppPage();
  });

  it('should display welcome message', () => {
    page.navigateTo();
    expect(page.getParagraphText()).toEqual('Welcome to app!!');
  });
});
